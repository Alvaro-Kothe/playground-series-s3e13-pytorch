import yaml
import pickle
import torch
from torch.utils.data import DataLoader
from sklearn.model_selection import train_test_split
from src.data import MyDataset
from src.train import train, test
import argparse
from torch import nn
from src.model import NeuralNetwork
from src.utils import read_data, get_features_targets, create_label_encoder


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "--input",
        "-i",
        required=True,
        type=argparse.FileType("r"),
        help="Training data filepath",
    )
    parser.add_argument(
        "--test_size", default=0.2, type=float, help="Test proportion size"
    )
    parser.add_argument("--batch_size", default=64, type=int, help="Batch size size")
    parser.add_argument(
        "--model-config",
        default="config/model.yml",
        help="Model config yaml file",
    )
    parser.add_argument(
        "--learning_rate",
        default=0.001,
        type=float,
        help="Neural Network learning rate",
    )
    parser.add_argument(
        "--epochs",
        default=10000,
        type=int,
        help="Number of epochs to train the neural network",
    )
    parser.add_argument("--le_save_location", help="LabelEncoder Save Path")
    parser.add_argument("--model_location", help="Model state dict save location")

    args = parser.parse_args()

    df = read_data(args.input)
    features, targets = get_features_targets(df)

    with open(args.model_config) as f:
        model_config = yaml.safe_load(f)

    model = NeuralNetwork(**model_config)

    le, encoded_target = create_label_encoder(targets)

    X_train, X_test, y_train, y_test = train_test_split(
        features, encoded_target, test_size=args.test_size
    )

    train_dataset = MyDataset(X_train, y_train)
    train_loader = DataLoader(train_dataset, batch_size=args.batch_size, shuffle=True)
    test_dataset = MyDataset(X_test, y_test)
    test_loader = DataLoader(test_dataset)

    loss_fn = nn.CrossEntropyLoss()
    optimizer = torch.optim.SGD(model.parameters(), lr=args.learning_rate)

    for t in range(args.epochs):
        print(f"Epoch {t+1}\n-------------------------------")
        train(train_loader, model, loss_fn, optimizer)
        test(test_loader, model, loss_fn)
    print("Done!")

    torch.save(model.state_dict(), args.model_location)

    with open(args.le_save_location, "wb") as f:
        pickle.dump(le, f)


if __name__ == "__main__":
    main()
