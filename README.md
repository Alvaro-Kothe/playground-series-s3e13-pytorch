# playground-series-s3e13 pytorch 

Pytorch model for kaggle competition [playground-series-s3e13](https://www.kaggle.com/competitions/playground-series-s3e13)

## Run code

Train the model with:

```shell
python train_model.py -i data/train.csv --epochs 10000 --le_save_location models/le.pkl --model_location models/NN.pth --model-config config/model.yml
```

Create submission prediction with:

```shell
python predict.py --le-path models/le.pkl --input data/test.csv --model-location models/NN.pth --prediction-path submission.csv --model-config config/model.yml
```
