import yaml
import pickle
from src.model import NeuralNetwork
import argparse
import pandas as pd
from src.utils import read_data
import torch
import torch.nn.functional as F


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "--input",
        "-i",
        help="Input csv file Path",
        type=argparse.FileType("r"),
        required=True,
    )
    parser.add_argument("--le-path", help="LabelEncoder Path", required=True)
    parser.add_argument(
        "--model-location", help="Model state dict location", required=True
    )
    parser.add_argument(
        "--prediction-path", help="Prediction save location", required=True
    )
    parser.add_argument(
        "--model-config",
        default="config/model.yml",
        help="Model config yaml file",
    )

    args = parser.parse_args()

    with open(args.le_path, "rb") as file:
        le = pickle.load(file)

    df = read_data(args.input)

    with open(args.model_config) as f:
        model_config = yaml.safe_load(f)

    model = NeuralNetwork(**model_config)

    model.load_state_dict(torch.load(args.model_location))

    model.eval()

    logits = model(torch.tensor(df.values, dtype=torch.float))

    _, indexes = F.softmax(logits, 1).max(1)

    predicted_prognosis = le.inverse_transform(indexes)

    submission = pd.DataFrame({"id": df.index, "prognosis": predicted_prognosis})

    submission.to_csv(args.prediction_path, index=False)


if __name__ == "__main__":
    main()
