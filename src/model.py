from torch import nn
import torch.nn.functional as F


class NeuralNetwork(nn.Module):
    def __init__(self, hidden_size=512, output_size=11, p_dropout0=0.2, p_dropout1=0.2):
        super().__init__()
        self.dropout0 = nn.Dropout(p_dropout0)
        self.dropout1 = nn.Dropout(p_dropout1)
        self.linear0 = nn.Linear(64, hidden_size)
        self.linear1 = nn.Linear(hidden_size, hidden_size)
        self.linear2 = nn.Linear(hidden_size, output_size)

    def forward(self, x):
        x = self.dropout0(x)
        x = self.linear0(x)
        x = F.relu(x)
        x = self.dropout1(x)
        x = self.linear1(x)
        x = F.relu(x)
        logits = self.linear2(x)
        return logits
