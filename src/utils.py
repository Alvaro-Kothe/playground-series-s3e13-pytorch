import pandas as pd
from typing import Tuple
from sklearn.preprocessing import LabelEncoder
import numpy as np


def read_data(filepath) -> pd.DataFrame:
    df = pd.read_csv(filepath, index_col="id")
    return df


def get_features_targets(df: pd.DataFrame) -> Tuple[np.ndarray, np.ndarray]:
    df = df.copy()
    targets = df.pop("prognosis").values
    features = df.values

    return features, targets


def create_label_encoder(targets) -> Tuple[LabelEncoder, np.ndarray]:
    le = LabelEncoder()
    encoded_target = le.fit_transform(targets)

    return le, encoded_target
